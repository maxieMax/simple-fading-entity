# Simple Fading Ability for Entities


This is just an ImpactJS plugin with built-in ability to fade in or out an entity and delete itself after a fade out.

Entity specific properties:

* span - defines the timespan in which the fading will take place
* fadeValue - three possible states. 'stop' => don't fade, 'in' => fade in, 'out' => fade out
* readyToKill - if this property is true and the Entity is not fading anymore (fadeValue == 'stop'), the entity will execute its kill() method

To use this entity in your game, just copy it into your ImpactJS entities folder and use it as it is, or let other entites extend the fading entity.

Fading an Entity on spawn should look something like this:

	spawnFading: function() {
		var settings = {
			span: 1, //time in seconds
			fadeValue: 'in', //let the image be faded in
			name: 'name', //name of the image file
		}
		this.spawnEntity(EntityFading, 1, 1, settings); //spawn entity at position x: 1 and y: 1
	}

For fading after spawning the entity, use the integrated setFade method. It needs the fadeValue ('in' or 'out'), span (fading timespan in seconds), readyToKill (bool).

	ent.setFade('out', 0.5, true);
	

